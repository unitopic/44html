import os, time, urllib.parse
from flask import Flask, render_template, send_from_directory, make_response, request, redirect, jsonify
from tools import size, format_bytes, chars
from replit import db, web
from markupsafe import escape
app = Flask("44html")
#app.config["SERVER_NAME"] = "44ht.ml"
#app.url_map.host_matching = True


# Login
login_header = open("templates/inc/header.html", "r")
login_footer = open("templates/inc/footer.html", "r")
login = f"{login_header.read()}\n{web.sign_in_snippet}\n<br />*more methods coming soon\n<br /><br />\n{login_footer.read()}"
del(login_header)
del(login_footer)



@app.route("/")
def main():
  if request.cookies.get("REPL_AUTH"):
    return redirect("/home")
  total_size = format_bytes(size("users"))
  rooms = 0
  for x in os.scandir("users"):
    if os.path.isdir(x):
      rooms += 1
  return render_template("index.html", size=f"{total_size[0]} {total_size[1]}", rooms=str(rooms))

#@app.errorhandler(404)
#def notfound():
#  return 

@app.route("/~<path:user>/<path:filename>", methods=["POST"])
def room(user, filename):
  form = request.form
  for x in form.keys():
    if x == "key" and form["key"] == os.getenv("KEY"):
      if os.path.isdir(f"users/{user}"):
        last = filename.split("/")
        if last[len(last)-1] == "":
          filename = filename + "index.html"
        if os.path.isfile(f"users/{user}/{filename}") == False:
          if os.path.isdir(f"users/{user}/{filename}") == False:
            if os.path.isfile(f"users/{user}/404.html"):
              return send_from_directory(f"users/{user}", "404.html"), 404
          else:
            if os.path.isfile(f"users/{user}/{filename}/index.html"):
            # test
              return redirect(f"/{filename}/")
            else:
              return "coming soon"
        return send_from_directory(f"users/{user}", filename)
      else:
        return render_template("user404.html"), 404
    #else:
      #return "404: User not found"

@app.route("/~<path:user>/", methods=["POST"])
def roomhome(user):
  return room(user, "index.html")

@app.route("/motd.txt")
def motd():
  return send_from_directory("", "motd.txt")



@app.route("/home")
@web.authenticated(login_res=login)
def home():
  #return str(db)
  for x in db.keys():
    if chars(x)[0] != "_":
      if str(db[x][0]) == web.auth.user_id:
        user = x
  return render_template("home.html", user=user)

@app.route("/files/<path:sub>")
@web.authenticated(login_res=login)
def files(sub):
  #return str(db)
  for x in db.keys():
    if chars(x)[0] != "_":
      if str(db[x][0]) == web.auth.user_id:
        suburl = urllib.parse.quote(sub)
        if sub != "":
          sub = "/" + sub
          suburl = "/" + suburl
        user = x
        root = f"{x}{sub}"
        folders = ""
        files = ""
        rooms = 0
        total_size = format_bytes(size(f"users/{x}"))
        for i in os.listdir(f"users/{x}{sub}"):
          rooms += 1
          if os.path.isdir(f"users/{x}{sub}/{i}"):
            file_size = format_bytes(size(f"users/{x}{sub}/{i}"))
            file_link = f"<a href=\"/files{suburl}/{urllib.parse.quote(i)}\">{escape(i)}/</a>"
            folders = folders + f"\n<tr><td>{file_link}</td><td title=\"Created at {time.ctime(os.path.getctime(f'users/{x}{sub}/{i}'))}\">{time.ctime(os.path.getmtime(f'users/{x}{sub}/{i}'))}</td><td>{file_size[0]} {file_size[1]}</td></tr>"
          elif os.path.isfile(f"users/{x}{sub}/{i}") and f"users/{x}{sub}/{i}" != f"users/{x}/44.json":
            file_size = format_bytes(os.path.getsize(f"users/{x}/{i}"))
            file_link = f"<a href=\"/edit{suburl}/{urllib.parse.quote(i)}\">{escape(i)}</a>"
            files = files + f"\n<tr><td>{file_link}</td><td title=\"Created at {time.ctime(os.path.getctime(f'users/{x}{sub}/{i}'))}\">{time.ctime(os.path.getmtime(f'users/{x}{sub}/{i}'))}</td><td>{file_size[0]} {file_size[1]}</td></tr>"

      files = folders + files

  return render_template("files.html", user=user, size=f"{total_size[0]} {total_size[1]}", files=files, root=root)

@app.route("/files/<path:sub>/")
def filess(sub):
  return files(sub)

@app.route("/files")
def filesr():
  return files("")

@app.route("/files/")
def filesrs():
  return files("")

@app.route("/edit/<path:filename>", methods=["GET", "POST"])
@web.authenticated(login_res=login)
def edit(filename):
  #return str(db)
  for x in db.keys():
    if chars(x)[0] != "_":
      if str(db[x][0]) == web.auth.user_id:
        if os.path.isfile(f"users/{x}/{filename}"):
          warning = ""
          if request.method == "POST":
            for k in request.form.keys():
              # Update file
              if k == "content":
                f = open(f"users/{x}/{filename}", "w+")
                f.write(request.form["content"])
                f.close()
                warning = "<p>Updated!</p>"
          f = open(f"users/{x}/{filename}", "r")
          c = escape(f.read())
          f.close()
          return render_template("edit.html", user=x, content=c, filename=filename, warning=warning)
        else:
          return "Doesn't exists"

@app.route("/logout")
def logout():
  if request.cookies.get("REPL_AUTH"):
    r = False
    for x in db["_old_cookies"]:
      if x == request.cookies.get("REPL_AUTH"):
        r = True
    if not r:
      db["_old_cookies"] = db["_old_cookies"].append(request.cookies.get("REPL_AUTH"))
  resp = make_response(redirect("/"))
  resp.set_cookie("REPL_AUTH", "", domain=".44html.sape.gq" ,expires=0)
  return resp


@app.route("/api/replit", methods=["POST"])
def replit():
  if request.method == "POST":
    #form = request.form
    if request.form["key"] == os.getenv("key"):
      for x in db.keys():
        if chars(x)[0] != "_":
          if str(db[x][0]) == str(request.form["id"]):
            return jsonify({"error": False, "name": x})
        #else:
          #return jsonify({"error": True, "code": 2})
    else:
      return jsonify({"error": True, "code": 1})
  else:
    return jsonify({"error": True, "code": 0})



app.run(host="0.0.0.0", port=8080)