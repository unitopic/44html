import os
from replit import db
from flask import request

def size(dir):
  size = 0
  for thing in os.scandir(dir):
    size += thing.stat().st_size
  return size



# From https://www.geeksforgeeks.org/python-split-string-into-list-of-characters/
def chars(word):
  return [char for char in word]



# Adapted from https://stackoverflow.com/questions/12523586/python-format-size-application-converting-b-to-kb-mb-gb-tb#49361727
def format_bytes(size):
  # 2**10 = 1024
  power = 2**10
  n = 0
  power_labels = {0 : '', 1: 'kilo', 2: 'mega', 3: 'giga', 4: 'tera'}
  while size > power:
    size /= power
    n += 1
  return size, power_labels[n]+'bytes'



#def logged():
#  if request.cookies.get("REPL_AUTH"):
#    for x in db["_old_cookies"]:
#      if x == request.cookies.get("REPL_AUTH"):
#        return False
#    return True
#  else:
#    return False